# ColorDemo Example and Training Content

The `colordemo <https://gitlab.com/rackn/example-content/-/tree/master/colordemo>`_
example content has been merged in to the larger `rackn/example-content <https://gitlab.com/rackn/example-content>`_
repo.  Please update any bookmarks, links, or references for it to point to the following
location:

  * https://gitlab.com/rackn/example-content/-/tree/master/colordemo

